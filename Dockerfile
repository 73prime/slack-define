FROM python:3.8-slim-buster

WORKDIR /app

COPY defineword/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY ./defineword/. .

CMD [ "python3", "flaskdriver.py"]


